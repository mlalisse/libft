/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/24 23:39:08 by mlalisse          #+#    #+#             */
/*   Updated: 2014/04/25 17:49:09 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/*
** unistd.h for read, write
** stdlib for free, malloc and defines
** string.h is only included because we want size_t to be defined
*/

#ifndef LIBFT_H
# define LIBFT_H

/*
**		{{ ctype.h
*/

int		ft_isalpha(int);
int		ft_isdigit(int);
int		ft_isalnum(int);
int		ft_isprint(int);
int		ft_isascii(int);
int		ft_toupper(int);
int		ft_tolower(int);

/*
**		ctype.h }}
**
**		{{ string.h
*/

/*
**		memchr -- locate byte in byte string
*/
void	*ft_memchr(const void *, int, size_t);

/*
**		memcmp -- compare byte string
*/
int		ft_memcmp(const void *, const void *, size_t);

/*
**		memcpy -- copy memory area
*/
void	*ft_memcpy(void *, const void *, size_t);

/*
**		memccpy -- copy string until character found
*/
void	*ft_memccpy(void *, const void *, int, size_t);

/*
**		memmove -- copy byte string
*/
void	*ft_memmove(void *, const void *, size_t);

/*
**		memset -- fill a byte string with a byte value
*/
void	*ft_memset(void *, int, size_t);

/*
**		strcat, strncat, strlcat -- concatenate strings
*/
char	*ft_strcat(char *, const char *);
char	*ft_strncat(char *, const char *, size_t);
size_t	ft_strlcat(char *, const char *, size_t);

/*
**		strchr, strrchr -- locate character in string
*/
char	*ft_strchr(const char *, int);
char	*ft_strrchr(const char *, int);

/*
**		strcmp, strncmp -- compare strings
*/
int		ft_strcmp(const char *, const char *);
int		ft_strncmp(const char *, const char *, size_t);

/*
**		strcpy, strncpy -- copy strings
*/
char	*ft_strcpy(char *, const char *);
char	*ft_strncpy(char *, const char *, size_t);

/*
**		strlen -- find length of string
*/
size_t	ft_strlen(const char *);

/*
**		strnstr, strstr -- locate a substring in a string
*/
char	*ft_strstr(const char *, const char *);
char	*ft_strnstr(const char *, const char *, size_t);

/*
**		strdup -- save a copy of a string
*/
char	*ft_strdup(const char *);

/*
**		bzero -- write zeroes to a byte string
*/
void	ft_bzero(void *, size_t);

/*
**		string.h }}
**
**		{{ string.h additions
*/

/*
**		strjoin, strjoinl, strjoinr, strjoinlr -- join strings
*/
char	*ft_strjoin(char const *, char const *);
char	*ft_strjoinl(char *, char const *);
char	*ft_strjoinr(char const *, char *);
char	*ft_strjoinlr(char *, char *);

/*
**		memjoin -- join byte strings
*/
void	*ft_memjoin(void *, size_t, void *, size_t);

/*
**		strsub -- get a byte string substring
*/
char	*ft_strsub(char const *, unsigned int, size_t);

/*
**		strsplit -- split the string into substring
**		like PHP explode. The resulting array has a NULL pointer at the end
*/
char	**ft_strsplit(char const *s, char c);

/*
**		strtrim -- return a new string without the '\t', ' ' at the start/end
*/
char	*ft_strtrim(char const *s);

/*
**		memalloc, memdup, memdel -- allocate, duplicate or free memory
*/
void	*ft_memalloc(size_t);
void	*ft_memdup(void *, size_t);
void	ft_memdel(void **);

/*
**		strnew, strdel -- allocate, duplicate or free string
*/
char	*ft_strnew(size_t);
void	ft_strdel(char **);

/*
**		strclr -- clear a string
*/
void	ft_strclr(char *);

/*
**		strequ, strnequ -- strcmp, strncmp variants : return 1 or 0
*/
int		ft_strequ(char const *, char const *);
int		ft_strnequ(char const *, char const *, size_t);

/*
**		striter, striteri -- Apply the function to each char of the string
*/
void	ft_striter(char *, void (*f)(char *));
void	ft_striteri(char *, void (*f)(unsigned int, char *));

/*
**		strmap, strmapi -- Map the function to each char and return a new array
*/
char	*ft_strmap(char const *, char (*f)(char));
char	*ft_strmapi(char const *, char (*f)(unsigned int, char));

/*
**		string.h additions }}
**
**		{{ print functions
**		NOTE : Each one is a syscall
*/

/*
**		putchar, putchar_fd -- print a char to the file descriptor or 0
*/
void	ft_putchar(char);
void	ft_putchar_fd(char, int);

/*
**		putstr, putstr_fd -- print a string to the file descriptor or 0
*/
void	ft_putstr(char const *);
void	ft_putstr_fd(char const *, int);

/*
**		putendl, putendl_fd -- print a string and a newline
*/
void	ft_putendl(char const *);
void	ft_putendl_fd(char const *, int);

/*
**		putnbr, putnbr_fd -- print a number
*/
void	ft_putnbr(int);
void	ft_putnbr_fd(int, int);

void	ft_puthex(void *);

/*
**		print functions }}
**
**		{{ tab functions
*/

size_t	ft_tablen(char **);

char	**ft_tabpush(char **, char *);

char	**ft_tabjoin(char **, char **);
char	**ft_tabjoinr(char **, char **);
char	**ft_tabjoinl(char **, char **);
char	**ft_tabjoinlr(char **, char **);

/*
**		tab functions }}
**
**		{{ list functions
*/

typedef struct		s_list
{
	void			*content;
	struct s_list	*next;
}					t_list;

t_list	*ft_lstnew(void *);

void	ft_lstfree(t_list *);


void	ft_lstpush(t_list **, t_list *);
void	*ft_lstpop(t_list **list);

void	ft_lstadd(t_list **, t_list *);

void	ft_lstiter(t_list *, void (*f)(t_list *));
t_list	*ft_lstmap(t_list *, t_list * (*f)(t_list *)); 

size_t	ft_lstlen(t_list *lst);

/*
**		list functions }}
**
**		{{ dblist functions
**		(Doublement chainee)
*/

typedef struct			s_dblist
{
	void				*content;
	struct s_dblist		*next;
	struct s_dblist		*prev;
}						t_dblist;


t_dblist	*ft_dblstnew(void *content);

void		ft_dblstadd(t_dblist **head, t_dblist *new);

/*
**		dblist functions }}
**
**		{{ queue functions
*/

typedef struct	s_queue
{
	t_list		*front;
	t_list		*back;
}				t_queue;

void	ft_queuepush(t_queue *queue, void *new);
void	*ft_queuepop(t_queue *queue);

/*
**		queue functions }}
**
**		{{ stdlib functions
*/

/*
**		realloc -- make a bigger buffer
**		(Unlike libc's realloc, we need to define the original size)
*/
void	*ft_realloc(void *, size_t, size_t);

/*
**		atoi -- convert ASCII string to integer
*/
int		ft_atoi(const char *);

/*
**		itoa -- convert integer to ASCII
*/
char	*ft_itoa(int);

/*
**		stdlib functions }}
*/

/*
**		get_next_line -- get a line from the file (defined by file descriptor)
**		BUF_SIZE is how much we read and store on each read() call
*/
# define BUF_SIZE 512
int		get_next_line(int const, char**);

#endif /* !LIBFT_H */
