/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 21:59:34 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:14:24 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*t;
	char	*t_saved;

	if (s == NULL || f == NULL)
		return (NULL);
	t = ft_strnew(ft_strlen(s));
	t_saved = t;
	while (*s != '\0')
		*t++ = (*f)((char)*s++);
	return (t_saved);
}
