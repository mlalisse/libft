/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 00:11:34 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:35:28 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	ft_strparts(char const *s, char c)
{
	int		parts;

	parts = 0;
	while (*s != '\0')
	{
		while (*s == c)
			s++;
		if (*s != '\0')
			parts++;
		while (*s != '\0' && *s != c)
			s++;
	}
	return (parts);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**tab;
	int		part;
	int		i;

	part = 0;
	tab = (char **)malloc((ft_strparts(s, c) + 1) * sizeof(char *));
	while (*s != '\0')
	{
		i = 0;
		while (*s == c)
			s++;
		while (s[i] != c && s[i] != '\0')
			i++;
		if (i > 0)
			tab[part++] = ft_strsub(s, 0, i);
		s += i;
	}
	tab[part] = NULL;
	return (tab);
}
