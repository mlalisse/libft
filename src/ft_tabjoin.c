/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 04:26:00 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:09:04 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_tabjoin(char **tab, char **tab2)
{
	char	**ret;
	char	**tmp;

	ret = malloc(sizeof(char*) * (ft_tablen(tab) + ft_tablen(tab2) + 1));
	tmp = ret;
	while (*tab)
		*tmp++ = ft_strdup(*tab++);
	while (*tab2)
		*tmp++ = ft_strdup(*tab2++);
	*tmp = NULL;
	return (ret);
}

char	**ft_tabjoinl(char **tab, char **tab2)
{
	char	**ret;

	ret = ft_tabjoin(tab, tab2);
	free(tab);
	return (ret);
}

char	**ft_tabjoinr(char **tab, char **tab2)
{
	char	**ret;

	ret = ft_tabjoin(tab, tab2);
	free(tab2);
	return (ret);
}

char	**ft_tabjoinlr(char **tab, char **tab2)
{
	char	**ret;

	ret = ft_tabjoin(tab, tab2);
	free(tab);
	free(tab2);
	return (ret);
}
