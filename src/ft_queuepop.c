/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuepop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/10 22:49:54 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:25:22 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_queuepop(t_queue *queue)
{
	t_list	*tmp;
	void	*content;

	tmp = queue->front;
	if (tmp == NULL)
		return (NULL);
	content = tmp->content;
	if (tmp->next)
		queue->front = tmp->next;
	else
	{
		queue->front = NULL;
		queue->back = NULL;
	}
	free(tmp);
	return (content);
}
