C Forty two library
===================

Basic C library only based on read(2), write(2), free(3) and malloc(3)

All function names start by "ft_" and all files must be compliant to the 42 code standart.
