/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dblstnew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfrere <tfrere@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 12:31:47 by tfrere            #+#    #+#             */
/*   Updated: 2014/03/26 12:37:49 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dblist	*ft_dblstnew(void *content)
{
	t_dblist	*list;

	list = ft_memalloc(sizeof(t_list));
	list->content = content;
	return (list);
}
