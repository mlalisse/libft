/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabpush.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 05:27:52 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:20:39 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_tabpush(char **tab, char *el)
{
	char	**ret;
	char	**tmp;
	int		len;

	len = ft_tablen(tab);
	tmp = malloc(sizeof(char*) * (len + 2));
	ret = malloc(sizeof(char*) * (len + 2));
	while (*tab)
		*tmp++ = ft_strdup(*tab++);
	ret[len] = el;
	ret[len + 1] = NULL;
	return (ret);
}

char	**ft_tabpushf(char **tab, char *el)
{
	char	**ret;

	ret = ft_tabpush(tab, el);
	free(tab);
	return (ret);
}
