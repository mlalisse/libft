/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 17:16:23 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/10 20:01:42 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstfree(t_list *list)
{
	t_list	*current;

	current = list;
	while (current != NULL)
	{
		current = current->next;
		free(list);
		list = current;
	}
}
