/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuepush.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/10 22:48:52 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:28:30 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_queuepush(t_queue *queue, void *new)
{
	if (!queue->front)
	{
		queue->back = ft_lstnew(new);
		queue->front = queue->back;
	}
	else
	{
		queue->back->next = ft_lstnew(new);
		queue->back = queue->back->next;
	}
}
