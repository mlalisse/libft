/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 23:11:29 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:35:10 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strsub(char const *s1, unsigned int start, size_t len)
{
	char	*s2;

	s2 = malloc(sizeof(char) * (len + 1));
	while (len--)
		s2[len] = s1[(size_t)start + len];
	return (s2);
}
