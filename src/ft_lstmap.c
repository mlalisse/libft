/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 17:29:56 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/30 04:02:20 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*out;

	out = NULL;
	if (f == NULL)
		return (NULL);
	while (lst != NULL)
	{
		ft_lstpush(&out, f(lst));
		lst = lst->next;
	}
	return (out);
}
