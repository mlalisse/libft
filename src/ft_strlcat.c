/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/10 20:00:57 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:14:52 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	r;

	i = 0;
	r = ft_strlen(dst);
	if (r > size)
		r = size;
	r += ft_strlen(src);
	while (dst[i] != '\0' && (i + 1) < size)
		i++;
	if ((i + 1) >= size)
		return (r);
	while (*src != '\0' && (i + 1) < size)
		dst[i++] = *src++;
	dst[i] = '\0';
	return (r);
}
