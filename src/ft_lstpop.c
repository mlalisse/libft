/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 20:49:14 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/11 22:51:28 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_lstpop(t_list **list)
{
	void	*content;
	t_list	*tmp;

	if (!list || !*list)
		return (NULL);
	tmp = *list;
	content = tmp->content;
	*list = tmp->next;
	free(tmp);
	return (content);
}
