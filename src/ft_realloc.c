/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 05:51:06 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:36:55 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_realloc(void *s, size_t len, size_t size)
{
	void	*ret;

	ret = malloc(size);
	ft_memcpy(ret, s, len);
	if (len > 0)
		free(s);
	return (ret);
}
