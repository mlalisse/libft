/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 01:09:52 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:43:39 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t	i;
	int		delta;

	i = 0;
	delta = 0;
	while (i < n && delta == 0)
	{
		delta = ((unsigned char *)s1)[i] - ((unsigned char *)s2)[i];
		i++;
	}
	return (delta);
}

int		ft_fmemcmp(const void *s1, const void *s2, size_t n)
{
	int		delta;

	if (n & 1 && (n-- || 1))
		delta = (char)(*((unsigned char*)s1++) - *((unsigned char*)s2++));
	if (n & 2 && ((n -= 2) || 1))
	{
		delta = (char)(*((unsigned short*)s1) - *((unsigned short*)s2));
		s1 += 2;
		s2 += 2;
	}
	if (n & 4 && ((n -= 4) || 1))
	{
		delta = (char)(*((unsigned int*)s1) - *((unsigned int*)s2));
		s1 += 4;
		s2 += 4;
	}
	while (n && ((n -= 8) || 1))
	{
		delta = (char)(*((unsigned long*)s1) - *((unsigned long*)s2));
		s1 += 8;
		s2 += 8;
	}
	return (delta);
}
