/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dblstadd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfrere <tfrere@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 12:31:05 by tfrere            #+#    #+#             */
/*   Updated: 2014/03/26 12:31:24 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_dblstadd(t_dblist **head, t_dblist *new)
{
	if (*head)
		(*head)->prev = new;
	new->next = *head;
	*head = new;
}
