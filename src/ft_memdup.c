/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 02:16:33 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:30:45 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memdup(void *s1, size_t n)
{
	char	*s;

	s = (char*)malloc(n);
	while (n-- > 0)
		((unsigned char*)s)[n] = ((unsigned char*)s1)[n];
	return ((void*)s);
}
