/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 01:10:00 by mlalisse          #+#    #+#             */
/*   Updated: 2014/03/27 21:37:08 by tfrere           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	while (n-- > 0)
		((unsigned char*)s1)[n] = ((unsigned char*)s2)[n];
	return (s1);
}

void	*ft_fmemcpy(void *s1, const void *s2, size_t n)
{
	if (n & 1 && (n-- || 1))
		*((unsigned char*)s1++) = *((unsigned char*)s2++);
	if (n & 2 && ((n -= 2) || 1))
	{
		*((unsigned short*)s1) = *((unsigned short*)s2);
		s1 += 2;
		s2 += 2;
	}
	if (n & 4 && ((n -= 4) || 1))
	{
		*((unsigned int*)s1) = *((unsigned int*)s2);
		s1 += 4;
		s2 += 4;
	}
	while (n && ((n -= 8) || 1))
	{
		*((unsigned long*)s1) = *((unsigned long*)s2);
		s1 += 8;
		s2 += 8;
	}
	return (s1);
}
